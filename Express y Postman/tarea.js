const config = require('./config');
const express = require('express');
const listaCelulares = require('./data');
const funciones = require('./misFunciones');

const app = express()

app.get("/lista-celulares", (req, res) => {
    res.status(200).json(listaCelulares);
})

app.get("/mitad-lista-cel", (req, res) => {
    res.status(200).json(funciones.listaMitadArray());
})  

app.get("/precio-mas-bajo", (req, res) => {
    res.status(200).json(funciones.precioMasBajo());
})  

app.get("/precio-mas-alto", (req, res) => {
    res.status(200).json(funciones.precioMasAlto());
})  

app.get("/agrupa-gama", (req, res) => {
    res.status(200).json(funciones.agrupaGama());
})  

app.listen(config.port, function () {
    console.log(`Escuchando el puerto ${config.port}`);
})