const listaCelulares = [
    {
        marca : "Iphone",
        gama : "Alta",
        modelo : "Iphone 12",
        precio : 1500,
    },
    {
        marca : "Samsung",
        gama : "Alta",
        modelo : "S20",
        precio : 1000,
    },
    {
        marca : "Xiaomi",
        gama : "Media",
        modelo : "P8 PRO",
        precio : 600,
    },
    {
        marca : "Motorola",
        gama : "Baja",
        modelo : "M10",
        precio : 300,
    },
    {
        marca : "Motorola",
        gama : "Baja",
        modelo : "M5",
        precio : 50,
    }
]

module.exports = listaCelulares