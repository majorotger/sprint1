
const listaCelulares = require('./data');
let mitadListaCel = [];


const cantMitadArray = (listaCelulares.length)/2;

function listaMitadArray() {
    const cantMitadArray = (listaCelulares.length)/2;
    mitadListaCel = listaCelulares.slice(0,cantMitadArray);
    return mitadListaCel;
}


function precioMasBajo() {
    let celAux = listaCelulares[0];
    listaCelulares.forEach (cel => {
        if (cel.precio < celAux.precio) celAux = cel;
    });
    return celAux;
}

function precioMasAlto() {
    let celAux = listaCelulares[0];
    listaCelulares.forEach (cel => {
        if (cel.precio > celAux.precio) celAux = cel;
    });
    return celAux;
}

function agrupaGama(){
    let gamaAlta = [];
    let gamaMedia = [];
    let gamaBaja = [];
    listaCelulares.forEach(cel => {
        if (cel.gama === "Alta"){
            gamaAlta.push(cel);
        }else if (cel.gama === "Media"){
            gamaMedia.push(cel);
        }else if (cel.gama === "Baja"){
            gamaBaja.push(cel);
        }
    });
    return gamas = [gamaAlta, gamaBaja, gamaMedia];
}


module.exports = {listaMitadArray, precioMasBajo, precioMasAlto, agrupaGama}