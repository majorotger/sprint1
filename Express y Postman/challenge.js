var express = require('express');
var app = express();

class Persona {
    constructor (nombre, apellido) {
        this.nombre = nombre;
        this.apellido = apellido;
    }
}

let pablo = new Persona("Pablo", "Secolo")
let agustin = new Persona("Agustin", "del Monte")
let antonella = new Persona("Antonella", "Piaggio")

const listadoAlumnos = [pablo, agustin, antonella]
let nuevoAlumno = {nombre: "Majo", apellido: "Rotger"};

app.post('/alumnos', function (req, res){
    listadoAlumnos.push(nuevoAlumno);
    res.send(`Usuario ${nuevoAlumno.nombre} agregado`)
});


/*for (let index = 0; index < listadoAlumnos.length; index++){
    const alumno = listadoAlumnos[index];
     app.get('/alumnos', function (req, res) {
        res.send(alumno);
      });;
}*/


app.get('/alumnos', function (req, res) {
    res.send(listadoAlumnos);
});

app.listen(3000, function () {
    console.log('Escuchando el puerto 3000!');
  });
  