document.getElementById("btn-verificar").addEventListener("click", function() {
const edadMin = 18
const fechaActual = 20210708
let nom = document.getElementById("nombre").value;
let ap= document.getElementById("apellido").value;
let edad = document.getElementById("edad").value;
let licencia = document.getElementById("licencia").value;
let fecha_exp = document.getElementById("fecha_expiracion").value;
if (nom == "" || ap == "" || edad == "" || fecha_exp == "")
{
     console.log("No podemos continuar con algun valor faltante")
}
else if (parseInt(edad) < edadMin || licencia == "no" || parseInt(fecha_exp) < fechaActual)
{
    console.log("la persona no esta habilitada para conducir")
}
else {
     console.log("la persona si esta habilitada para conducir")
}
})
      