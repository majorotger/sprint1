let arrayUsuarios = [];
​
document.getElementById("btn-registrar").addEventListener("click", function() {
    const nombre         = document.getElementById("nombre").value;
    const apellido       = document.getElementById("apellido").value;
    const email          = document.getElementById("email").value;
    const pais           = document.getElementById("pais").value;
    const clave          = document.getElementById("clave").value;
    const claveRepetida  = document.getElementById("repetir-clave").value;
​
    if (nombre == '' || apellido == '' || email == '' || pais == '' || clave == '' || claveRepetida == ''){
        alert('Hay campos vacíos, no puede continuar');
        return;
    } if (clave != claveRepetida) {
        alert('Las contraseñas deben ser iguales, intente nuevamente');
        return;
    }
    for (let i = 0; i < arrayUsuarios.length; i++){
        if (email == arrayUsuarios[i].email) {
            alert('No puede registrar un email repetido');
            return;
        }
    }
    let usuario = new Usuario(nombre, apellido, email, pais, clave, claveRepetida)
    arrayUsuarios.push(usuario);
})
​
document.getElementById("btn-ingresar").addEventListener("click", function() {
    const email = document.getElementById("email-login").value;
    const clave = document.getElementById("clave-login").value;
​
    // Si indiceUser == -1, no se encontró ningún usuario válido
    const indiceUser = arrayUsuarios.findIndex(function(elemento) {
        return elemento.email == email && elemento.clave == clave;
    });
​
    if (indiceUser != -1) {
        alert('¡Ingresaste satisfactoriamente!');
        return indiceUser;
    } else {
        alert('Usuario inválido');
        return false;
    }
})