const swaggerJsDoc = require('swagger-jsdoc');

const swaggerUI = require('swagger-ui-express');



const swaggerOptions = {
    swaggerDefinition: {
      info: {
        title: 'Acamica API',
        version: '1.0.0'
      }
    },
    apis: ['./app.js'],
  };
  
  const swaggerDocs = swaggerJsDoc(swaggerOptions);

  app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerDocs));