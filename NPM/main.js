const coolImages = require("cool-images");
const fs = require("fs");
const moment = require("moment");

//URL de una imagen aleatoria
//const imagen = coolImages.one(); 

//array de URL de imagenes aleatorias
const listadoDeUrl = coolImages.many(600, 800, 10);

//console.log("la URL de la imagen " i + "es: " + listadoDeUrl[i]);
const theDate = new Date();
const logDate = moment(theDate).format('LLLL');

const mostrar_y_GuardarListadoUrl= (listadoDeUrl) => {
      listadoDeUrl.forEach(function (url, idx){ 
        console.log(`La URL de la imagen ${idx} es ${url}`);
        fs.appendFileSync("./log.txt", logDate + " " + listadoDeUrl[idx] + "\n");
    })
}

mostrar_y_GuardarListadoUrl(listadoDeUrl);

