const config = require('./config');
const express = require('express');
const autores_libros = require('./data');

const app = express();

function validarIdAutor(req, res, next) {
    const idAutor = parseInt(req.params.id);

    if ( !Number.isInteger(idAutor))
    {
        res.status(422).json({msg: "El id debe ser un número entero"})
        return
    }
    else {
        next()
    }
}

function validarExisteAutor(req, res, next){
    const idAutor = parseInt(req.params.id);
    const autor = autores_libros.find(autor => autor.id === idAutor)

    if (autor) {
        res.status(200).json(autor);
    } else {
        res.status(400).json(`No se encuentra el autor con id ${idAutor}`)
    }
}

function validarIdLibro(req, res, next) {
    const idLibro = parseInt(req.params.id);

    if ( !Number.isInteger(idLibro))
    {
        res.status(422).json({msg: "El id debe ser un número entero"})
        return
    }
    else {
        next()
    }
}

app.get('/autores', (req, res) => {
    const autor = autores_libros;
    
    res.status(200).json(autor);
})

app.post('/autores', (req, res) => {
    res.status(201).send('Ruta para crear un autor!')
})

app.get('/autores/:id', validarIdAutor, validarExisteAutor, (req, res) => {
})

app.delete('/autores/:id', validarIdAutor, (req, res) => {
    const idAutor = parseInt(req.params.id);
    const autor = autores_libros.find(autor => autor.id === idAutor)

    if (autor) {
        // elimiar el autor
        const posAutorId = autores_libros.indexOf(autor)
        autores_libros.splice(posAutorId, 1)
        res.json(autores_libros)
    } else {
        res.status(400).json(`No se encuentra el autor con id ${idAutor}`)
    }

})
    
app.put('/autores/:id', validarIdAutor, (req, res) => {
    const idAutor = parseInt(req.params.id);
    const autor = autores_libros.find(autor => autor.id === idAutor)

    if (autor) {
        // modificar el autor
        res.status(201).send('Ruta para modificar un autor!')
    } else {
        res.status(400).json(`No se encuentra el autor con id ${idAutor}`)
    }
    
})

app.get('/autores/:id/libros', validarIdAutor, (req, res) => {

    const idAutor = parseInt(req.params.id);
    const autor = autores_libros.find(autor => autor.id === idAutor)

    if (autor) {
        res.status(200).json(autor.libros);
    } else {
        res.status(400).json(`No se encuentra el autor con id ${idAutor}`)
    }
})

app.post('/autores/:id/libros', validarIdAutor, (req, res) => {

    const idAutor = parseInt(req.params.id);
    const autor = autores_libros.find(autor => autor.id === idAutor)

    if (autor) {
        res.status(200).json(`Ruta para crear un libro del autor ${autor.nombre} ${autor.apellido}!`);
    } else {
        res.status(400).json(`No se encuentra el autor con id ${idAutor}`)
    }
})

app.get('/autores/:id/libros/:idLibros', validarIdAutor, validarIdLibro, (req, res) => {

    const idAutor = parseInt(req.params.id);
    const autor = autores_libros.find(autor => autor.id === idAutor)

    if (autor) {
        const idLibro = parseInt(req.params.idLibros)
        console.log(req.params.idLibros)
        const libro = autores_libros.find(libro => libro.id === idLibro)
        const posLibroId = autores_libros.indexOf(libro)

        if (libro) {
            res.status(200).json(autor.libros[posLibroId]);
        } else {
            res.status(400).json(`No se encuentra el libro con id ${idLibro}`)
        }
    } else {
        res.status(400).json(`No se encuentra el autor con id ${idAutor}`)
    }
})

app.listen(config.port, function () {
    console.log(`Escuchando el puerto ${config.port}`);
})
