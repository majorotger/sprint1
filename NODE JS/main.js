//const hablar = require("./misFunciones")

//hablar("Hola Mundo!")

const fs = require(`fs`)

const funciones = require("./misFunciones")

//convierte las mayusculas en minusculas
funciones.hablarBajito("HOLA MUNDO")

//Muestra todos los archivos que tengo en la raiz
fs.readdir("./", (err, files) => {
    files.forEach(file => {
        console.log(file)
    })
} )