const calculadora = require("./calculator.js")
const fs = require('fs');

//console.log(calculadora.sumar(1,2))

//console.log(calculadora.restar(4,2))


function mostrar_y_guardar(res){
    console.log(res);
    fs.appendFileSync("./log.txt", res + "\n");
}

let a = 10;
let b = 5;

let resultado;

resultado = `${a} + ${b} = ${calculadora.sumar(a,b)}`;
mostrar_y_guardar(resultado);

resultado = `${a} - ${b} = ${calculadora.restar(a,b)}`;
mostrar_y_guardar(resultado);

resultado =`${a} X ${b} = ${calculadora.multiplicar(a,b)}`;
mostrar_y_guardar(resultado);

resultado =`${a} / ${b} = ${calculadora.dividir(a,b)}`
mostrar_y_guardar(resultado);
