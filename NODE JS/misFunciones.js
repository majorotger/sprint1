function hablar(algo){
    console.log(algo)
}

function hablarBajito(algo) {
    console.log(algo.toLowerCase())
}

//tengo que exportar esto para poder usarlo en otro archivo

//esta linea se puede hacer solo una vez
//module.exports = hablar


//para exportar todo lo hago asi:
module.exports = {hablar, hablarBajito}
