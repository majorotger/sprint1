
const mascotas = ["perro", "gato", "pez"]
mascotas.push("pajaro")

//console.log(mascotas)

//function imprimirUnaMascota(mascota){
//    console.log(mascota)
//}

const mostrarMascotas = (listadoDeMascotas) => {
    //para recorrer el array usaremos una funcion de los array que se llama foreach, en lugar de for o while
  listadoDeMascotas.forEach(function (mascota, idx){ //el foreach me permite tener el indice
        console.log(`${idx} - ${mascota}`)
    })
}

/**
 * podria pasarle la funcion completa dentro del foreach si es que no necesito llamarla desde otro lado
 * con esto puedo eliminar el nombre (ver arriba)
 * const mostrarMascotas = (listadoDeMascotas) => {
        listadoDeMascotas.forEach(imprimirUnaMascota)
}
 * 
 */

mostrarMascotas(mascotas)