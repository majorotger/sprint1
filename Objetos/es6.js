//otra forma de crear objetos segun ECMA Scrip 6 

//"CLASE" PERSONA
//CONSTRUCTOR
class Persona {
    constructor (nombre, anyoNac) {
        this.nombre = nombre;
        this.anyoNac = anyoNac;
    }
    saludar() {
        return "Hola soy " + this.nombre
    }
    static diferenciaAnyos(persona1, persona2) {
        return "La diferencia de edad es: " + (persona1.anyoNac - persona2.anyoNac) + " años"
    }    
}


var leo = new Persona("Leo", 2000)
//puedo agregar metodos propios a cada uno de los objetos    
    leo.decirHola = function () {
        return "Hola !!!"
    }
var tizi = new Persona("Tiziano", 2010)

//"CLASE" DIRECTIVO "EXTIENDE" A PERSONA
//CONSTRUCTOR
class Directivo extends Persona {
    constructor(nombre, colegio) {
        super(nombre, 1900);
        this.colegio = colegio; 
    }
    despedirse (){
        return "Bye, lo saluda el directivo " + this.nombre + " del colegio " + this.colegio
    }
}


//USO DE MIS OBJETOS
var directorJuan = new Directivo("Juan", "ISFT151")

var leo = new Persona("Leo", 1990)
var tizi = new Persona("Tiziano", 2010)

var mariaDirectora = new Directivo("Maria", "EEM12")

var dai = new Directivo("Dai", "Esc. 31")
dai.anyoNac = 1995