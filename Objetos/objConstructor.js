//otra forma de crear objetos usando constructor

//"CLASE" PERSONA
//CONSTRUCTOR
var Persona = function (nombre, anyoNac) {
    this.nombre = nombre;
    this.anyoNac = anyoNac;
    //this.saludar = function(){
    //    return "Hola soy " + this.nombre
    //}
    //cada instancia que creamos de este objeto tiene el metodo "saludar" agregado, no es lo ideaL. Por esto 
    //la sacamos afuera
}

//Metodos estaticos me sirve para llamar un metodo sin crear una instancia
Persona.diferenciaAnyos = function (persona1, persona2) {
    return "La diferencia de edad es: " + (persona1.anyoNac - persona2.anyoNac) + " años"
}

//Con esto logro que el metodo "saludar" ya no este en cada una de las instancias
Persona.prototype.saludar = function(){
   return "Hola soy " + this.nombre
}

var leo = new Persona("Leo", 2000)
//puedo agregar metodos propios a cada uno de los objetos    
    leo.decirHola = function () {
        return "Hola !!!"
    }
var tizi = new Persona("Tiziano", 2010)

//"CLASE" DIRECTIVO "EXTIENDE" A PERSONA
//CONSTRUCTOR
var Directivo = function (nombre, colegio) {
    Persona.call(this, nombre, 1900);
    this.colegio = colegio; 
}

//al solucionar el tema de "saludar" en cada una de las instancias, directivo no podria llamar al metodo. Para esto hacemos
Directivo.prototype = Object.create(Persona.prototype);

Directivo.prototype.despedirse = function(){
    return "Bye, lo saluda el directivo " + this.nombre + " del colegio " + this.colegio
 }
 //porque cuando vemos que tiene "directivoJuan" este metodo figura como dentro del prototipo persona?

//USO DE MIS OBJETOS
var directorJuan = new Directivo("Juan", "ISFT151")

var leo = new Persona("Leo", 1990)
var tizi = new Persona("Tiziano", 2010)

var mariaDirectora = new Directivo("Maria", "EEM12")

var dai = new Directivo("Dai", "Esc. 31")
dai.anyoNac = 1995

