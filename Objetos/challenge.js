//"CLASE" PERSONA
//CONSTRUCTOR
class Persona {
    constructor (nombre, apellido, edad) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        
    }
    saludar() {
        return "Hola soy " + this.nombre + " " + this.apellido
    }
  
}

//"CLASE" ALUMNO "EXTIENDE" A PERSONA
//CONSTRUCTOR
class Alumno extends Persona {
    constructor(nombre, apellido, leng_estudiado) {
        super(nombre, apellido); 
        //con "super" llamo al constructor de la clase base
        this.lengEstudiado = leng_estudiado; 
    }
    despedirse (){
        return "Bye, lo saluda el alumno " + this.nombre + " " +  this.apellido + " estudiante de: " + this.lengEstudiado
    }
}

var pablo = new Alumno("Pablo", "Secolo", "Java Script")
var agustin = new Alumno("Agustin", "del Monte","C++")
var antonella = new Alumno("Antonella", "Piaggio", "Python")