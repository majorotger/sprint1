// porque se declara como "var"?
//titulo, consola y jugar son PROPIEDADES
var VideoJuego = {
    titulo:"",
    consola:"",
    jugar() {
        console.log("Estoy jugando a " + this.titulo + " en la consola " + this.consola);
    }
    //otra forma de escribir el metodo "jugar" es:
    //jugar: function () {
    //  console.log("Estoy jugando a " + this.titulo + " en la consola " + this.consola);
    //} 

}

//Creo una instancia de un objeto, usa un prototipo, hereda los datos de VideoJuego
//porque aca usa ";" y en la linea 21,22 no?
var Mario = Object.create(VideoJuego);
Mario.__init = function(titulo, version) {
    this.titulo = titulo;
    this.version = version;
}
Mario.consola = "Nintendo";
Mario.version = 0;

var marioUno = Object.create(Mario)
var marioTres = Object.create(Mario)
//esta es una forma
marioUno.titulo = "Mario, the first"
marioUno.version = 1
//para escribir menos codigo y hacerlo mejor, agrego la funcion __init y la uso
marioUno.__init("Mario, the first", 1)
marioTres.__init("Mario Bros 3", 3)

var Sonic = Object.create(VideoJuego)
Sonic.__init = function(titulo, version, consola) {
    this.titulo = titulo;
    this.version = version;
    this.consola = consola;
}

var sonic1 = Object.create(Sonic)
sonic1.__init("Sonic", 1, "Sega")