//"CLASE" PERSONA
//CONSTRUCTOR
class Persona {
    constructor (nombre, apellido, edad) {
      this.nombre = nombre;
      this.apellido = apellido;
      this.edad = edad;
    }
  fullname() {
      console.log("Mi nombre es " + this.nombre + " " + this.apellido);
  }

  es_mayor() {
      console.log(this.edad >= 18);
  }
  
}

let majo = new Persona ("Mara Jose", "Rotger", 37)
majo.fullname()
majo.es_mayor
