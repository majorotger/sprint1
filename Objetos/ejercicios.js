//PETAPP
class Perro {
    constructor (nombre, raza, edad, sexo, estadoAdopcion) {
        this.nombre = nombre;
        this.raza = raza;
        this.edad = edad;
        this.sexo = sexo;
        this.estadoAdopcion = estadoAdopcion;
    }
    
    setEstado (estado){
      this.estadoAdopcion = estado    
    }

    getEstado(){
        estadoActual = this.estadoAdopcion;
        return estadoActual;
        //return `El perro ${this.nombre} esta ${this.estadoAdopcion}`;
    }
}    

let estadoActual=""
let estadoAdopcion = ["En adopcion", "Proceso de adopcion", "Adoptado"]
let perrera = []
let enAdopcion = []
let procesoAdopcion = []
let adoptado = []
let estado=""
let perro1 = new Perro ("Pepe", "Pug", "Macho", 5, estadoAdopcion[0])
let perro2 = new Perro ("Vacuna", "Golden", "Hembra", 8, estadoAdopcion[0])
let perro3 = new Perro ("Sony", "Beagle", "Macho", 3, estadoAdopcion[0])
perrera.push(perro1,perro2,perro3);                                         // Creo array Perrera y meto los perros de ejemplo
listarPerros(perrera);


//////////////////////////////////////////////////////////////////////////////////
function cargarPerro (){
    
    do {
        let nombre = prompt("Ingrese el nombre del perro")
        while(!nombre){
            nombre = prompt("Debe ingresar un valor de nombre")
        }
        let raza = prompt("Ingrese la raza del perro")
        while(!raza){
            raza = prompt("Debe ingresar un valor de raza")
        } 
        let edad = prompt("Ingrese la edad del perro")
        while(!edad){
            edad = "se desconoce cantidad de"
        }
        let sexo = prompt("Ingrese el sexo del perro")
        while(!sexo || sexo!="macho" && sexo!="hembra"){
            sexo = prompt("Debe ingresar un valor de sexo correcto")
        } 
        let nuevoPerro = new Perro (nombre, raza, edad, sexo, estadoAdopcion[0]);
        perrera.push(nuevoPerro);
    } while (window.confirm("Desea seguir agregando perros a la perrera?"));
    listarPerros(perrera);
}

/////////////////////////////////////////////////////////////////////////////

function modificarEstado() { 
    let idaModificar = prompt("Ingrese el ID del perro que desea modificar el estado"); 
    while(!verificarID(idaModificar)){
        idaModificar = prompt("ID no identificado, por favor ingrese un ID correcto");
    }
    let nuevoEstado = prompt("Ingrese el nuevo estado \n1:En adopcion \n2:Proceso de adopcion \n3:Adoptado");
    while(!verificarEstado(nuevoEstado-1)){
        nuevoEstado = prompt("Ingrese un estado correcto:  \n1:En Adopcion \n2:En proceso de adopcion \n3:Adoptado");
    }
    while(verificarEstadoCorrecto(nuevoEstado, idaModificar)){
        nuevoEstado = prompt("El perro ya fue adoptado. Ingrese un estado correcto:  \n1:En Adopcion \n2:En proceso de adopcion \n3:Adoptado");
    }
    perrera[idaModificar].setEstado(estadoAdopcion[nuevoEstado-1]);
    listarPerros(perrera);
}

///////////////////////////////////////////////////////////////////////////
function listarPerros (filtrados) {
    let userHTML = "";
    filtrados.forEach(function(perro, indice){
        userHTML += `<p><b>[Perro ID ${indice}] Nombre: </b>${perro.nombre}, <b>Raza:</b> ${perro.raza}, <b>Sexo:</b> ${perro.edad}, <b>Edad:</b> ${perro.sexo} años, <b>Estado:</b> ${perro.estadoAdopcion}</p>`;
    });                                       
    document.getElementById("listarPerros").innerHTML = userHTML;  
}

///////////////////////////////////////////////////////////////////////////
function verificarID(idaModificar){
    if(idaModificar != ""){
    if(idaModificar < perrera.length && idaModificar >= 0){
        return true
    }else {
        return false
    }
    }
}

//////////////////////////////////////////////////////////////////////////
function verificarEstado(nuevoEstado){                          // Verifico que se ingrese uno de los 3 estados correctos.
    if (nuevoEstado >= 0 && nuevoEstado < estadoAdopcion.length){
        return true;
    }else{
        return false;
    }
}

////////////////////////////////////////////////////////////////////////
function verificarEstadoCorrecto (nuevoEstado, idaModificar){
    if (perrera[idaModificar].getEstado() == estadoAdopcion[2] && nuevoEstado-1 == 2){
        return true;
    }else{
        return false;
    }
}

///////////////////////////////////////////////////////////////////////
function filtrarEstado(estado){
     
    if(estado==5){
        listarPerros(perrera);
        enAdopcion = [];
        procesoAdopcion = [];
        adoptado = [];
    }else {
    for(let i=0; i < perrera.length; i++){
        const perrodelaPerrera = perrera[i];        
        if(perrodelaPerrera.estadoAdopcion===estadoAdopcion[0]){
            enAdopcion.push(perrodelaPerrera);
        }else if(perrodelaPerrera.estadoAdopcion===estadoAdopcion[1]) {
            procesoAdopcion.push(perrodelaPerrera); 
        }else if(perrodelaPerrera.estadoAdopcion===estadoAdopcion[2]){
            adoptado.push(perrodelaPerrera);
        }
    } 
    }
    console.log(estado);
    if(estado==0){
        listarPerros(enAdopcion);
        procesoAdopcion = [];
        adoptado = [];
    }else if(estado==1){
        listarPerros(procesoAdopcion);
        enAdopcion = [];
        adoptado = [];
    }else if(estado==2) {    
        listarPerros(adoptado);
        enAdopcion = [];
        procesoAdopcion = [];
    }
}

//OTRA FORMA DE FILTRAR
//function filtrarEstado(nuevoEstado){                                                    // Funcion que filtra el estado del perro
//    if (nuevoEstado == "5"){                                                            // La opcion "5" borra todos los filtros, muestra toda la perrera
//    listarPerros(perrera);
//    }else{
//    filtroAdoptado = perrera.filter(perro => perro.estadoAdopcion == estadoAdopcion[nuevoEstado]);
//    listarPerros(filtroAdoptado);                                                       // Vuelve a listarPerros con los arrays filtrados
//    }
//}