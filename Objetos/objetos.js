
const marcaYModeloMiAuto = "Peugeot Partner";

//voy a crear un objeto
const miAuto = {
    marca: "Peugeot",
    modelo: "Partner",
    anyo: 2020,
    dominio: "AB123CD",
    //el guion bajo se usa como convencion para proteger propiedades, es porque js no permite que sean privadas
    _alquilado: false,
    alquilar: function (){
        if (this._alquilado){
            throw new Error("No se puede alquilar")
        }
        this._alquilado = true;
        return this._alquilado;
    },
    devolver: function () {
        this._alquilado = false;
        return this._alquilado;
    }
}

const autoDeDolores = {
    marca: "Ford",
    modelo: "Fiesta",
    anyo: 2020,
    dominio: "AB124CD",
    alquilado: false,
    alquilar: function (){
        this.alquilado = true;
    }
}

const garage = [miAuto, autoDeDolores]

function publicarAuto(auto){
    //es mejor usar dentro de los parentesis ´Se vende el auto marca: ${this.marca}´
    console.log("Se vende el auto marca: " + this.marca)
}

//do {
//  let nuevoAuto = {}
//  nuevoAuto.marca = "Fiat"
//  nuevoAuto.modelo = "Ducato"
//  nuevoAuto.anyo = 1995
//  nuevoAuto.dominio = "12345"
//  nuevoAuto._alquilado = false
//  garage.push(nuevoAuto)
//} while (false);

//Para que el usuario ingrese los datos
do {
    let nuevoAuto = {}
    nuevoAuto.marca = prompt("Ingrese la marca de su auto")
    nuevoAuto.modelo = prompt("Ingrese la modelo de su auto")
    nuevoAuto.anyo = prompt("Ingrese el año de su auto")
    nuevoAuto.dominio = prompt("Ingrese el dominio de su auto")
    nuevoAuto._alquilado = false
    garage.push(nuevoAuto)
} while (window.confirm("Desea seguir agregando autos al garage?"));



for (let index = 0; index < garage.length; index++){
    const autoDelGarage = garage[index];
    publicarAuto(autoDelGarage);
}



